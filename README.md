# English Notes

## Alphabet
a b c d e f g h i j k l m n o p q r s t u v w x y z

## Vowels
a e i o u

## Vocabulary

- bowel -> Intestino
- kind -> type, class
- therefore -> por lo tanto
- sharp -> en punto
- foreinger -> extrangero/a
- overwhelm -> sobrecargar , asombrar, anonadar, abrumar
- stuff -> mis cosas
- staff -> el personal
- strolls -> paseos
- Scramble -> batidos
- gift -> regalo, presente, don, talento
- mammal -> mamirefo
- confident -> confiado
- fashion -> moda, manera, uso
- Pattern -> patrón
- Trend -> tendencia
- enphatic -> enfático/a
- remark -> observar, comentar, notar - observación, nota, comentario
- procedure -> procedimiento
- issue -> emitir, expedir - el asunto, el problema, la cuestión
- surename -> apellido
- lockdown -> cierre de emergencia
- advice -> consejo
- advise -> asesorar
- once -> una vez
- twice -> dos veces
- thrice ->  tres veces
- daughter -> hija
- springboard -> trampolín
- square -> cuadrado - cuadramos algo
- Shovel -> pala
- Shoveled -> apalear
- choose -> escoger
- bond -> ligadura , esclavo, garantizar
- enough -> suficiente

## Phrases

- So far so good -> Hasta aquyi no hay problema
- So on and so forth -> Y así sucesivamente
- Take it easy -> Tomalo con calma
- That's it -> Eso estodo
- by the way -> a propósito
- for instance -> por ejemplo
- Fasten your seatbelt -> Abroche el cinturón de seguridad
- thin line -> linea fina
- both sides of the coin -> ambos lados de la moneda
- said hi for me -> dile que saludos de mi parte
- Lots of work -> mucho trabajo
- let's find out -> vamos a averiguar


## Modal verbs
- lately -> ultimamente
- would love -> amaria
- migth -> podria


## Numbers

### Cardinal

- 999.999.999.999.999
- trillion.billion.million.thousand.hundred

### Ordinal

- First (1st)
- Second (2nd)
- third (3rd)
- Fourth (4th)
- Fifth
- Sixth
- Seventh
- Eighth
- Ninth
- Tenth
- Eleventh
- Twelfth
- Twentieth
- Twenty-first
- Thirtieth

## Body 
- https://www.spanishdict.com/guia/las-partes-del-cuerpo-en-ingles
### Inside
### Outside

## Nationalities

https://www.ef.com/ca/english-resources/english-grammar/nationalities/

## Questions

- https://www.aprendemasingles.com/preguntas-comunes-ingles/
- Who do you work for? -> para quien trabaja 
- what did make you decide to move to mexico?

## Sentences structure (Standartd)

### Structure

S + V + C

Subject (who doing action)

### Pronoons
	I	1st personal of the singular
	YOU	2nd 
	HE	3rd
	SHE	3
	IT	3
	WE	Plural
	YOU	p
	THEY	p

### Personal 
	Tangible
	Untangible
		the Situation is dangerous

### Verb To Be
	I	AM	I'M
	YOU	ARE	YOU're
	HE	IS	HE's
	SHE	IS	she's
	IT	IS	it's
	WE	ARE	We're
	YOU	ARE	you're
	THEY	ARE	they're
	
	
## Simple Paste / past
	I	was	not
	YOU	were	not	
	HE	was	not
	SHE	was	not
	IT	was	not
	WE	were	not
	YOU	were	not
	THEY	were	not
	
	
### Questions to be
	V to be + S + C
	
	

### Pronouns
	I	was	not
	YOU	were	not	
	HE	was	not
	SHE	was	not
	IT	was	not
	WE	were	not
	YOU	were	not
	THEY	were	not
	
### Nouns
	Others
	
S + To be + Nouns

### Plural
	1) Ending consonant +s
	
	" Plurar = Simple present 3rd singular
			Main Verbs "
			
	2) Ending on silent E
		Cake		Cakes
		Time	+ S	Times
		Rule		Rules
			
	3) Ending on y 

## Adjetives

### compartatives

### superlatives

## Verbs

